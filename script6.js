// Даний об'єкт employee. Додайте до нього властивості age і salary, не змінюючи початковий об'єкт (має бути створено новий об'єкт, який включатиме всі необхідні властивості). Виведіть новий об'єкт у консоль.

const employee = {
    name: 'Vitalii',
    surname: 'Klichko'
}

// function newEmployee (item){
//     let emp = Object.create(item)
//     emp.age = "51"
//     emp.salary = "100000"

// }

function newEmployee ({name,surname}){
    let emp = {
        firstName: name,
        lastName: surname,
        age: 51,
        salary: 100000
    }

    console.log(emp)
}

newEmployee(employee)

